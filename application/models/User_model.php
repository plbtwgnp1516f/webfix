<?php
Class User_Model extends MY_Model
{
	protected $_table ='tbl_pengguna';
	protected $return_type = 'array';

	protected $after_get = array('remove_sensitive_data');
	protected $before_create = array('prep_data');
	
	protected function remove_sensitive_data($data){
		unset($data['password']);
		return $data;
	}
	protected function prep_data($data){
		$data['password']=md5($data['password']);
		return $data;
	}
}
?>
