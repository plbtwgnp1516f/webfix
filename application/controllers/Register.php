<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//nama class harus sama dengan nama file dan diawali dengan huruf besar
class Register extends CI_Controller {

    public function index()
    {
          
            $this->load->model('User_model');
			$this->load->library('session');
			$data=array(
				'username'=>$this->input->post('username'),
				'password'=>$this->input->post('password'),
				'nama'=>$this->input->post('nama'),
				'email'=>$this->input->post('email'),
				'nomor_handphone'=>$this->input->post('nohandphone'),
			);
			
			$result=$this->User_model->adduser($data);
			if($result==TRUE)
			{
				$this->session->set_flashdata('msg', '<div class="alert alert-success text-center">Registrasi Berhasil ! Silahkan login untuk melanjutkan</div>'); 
				redirect("Login/index"); 
				} 
				else { 
				$this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">Email sudah ada</div>'); 
				redirect("Register/index"); 
				} 
			}
			
			

    public function signup(){
		$this->load->library('session');
		$this->load->view('v_register');
	}

}